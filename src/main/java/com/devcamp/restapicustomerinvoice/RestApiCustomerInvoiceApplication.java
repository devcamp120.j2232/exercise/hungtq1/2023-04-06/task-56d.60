package com.devcamp.restapicustomerinvoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiCustomerInvoiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiCustomerInvoiceApplication.class, args);
	}

}
