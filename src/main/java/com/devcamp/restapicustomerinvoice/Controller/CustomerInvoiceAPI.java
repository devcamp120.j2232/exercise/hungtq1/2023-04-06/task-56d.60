package com.devcamp.restapicustomerinvoice.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapicustomerinvoice.Controller.models.Customer;
import com.devcamp.restapicustomerinvoice.Controller.models.Invoice;
import com.devcamp.restapicustomerinvoice.service.InvoiceService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CustomerInvoiceAPI {
  @Autowired
  private InvoiceService invoiceService;

  @GetMapping("/invoices")
  public ArrayList<Invoice> getInvoiceList() {
    return invoiceService.getInvoiceList();
  }

  @GetMapping("/invoices/{index}")
  public Invoice getInvoiceByIndex(@PathVariable int index) {
    return invoiceService.getInvoiceByIndex(index);
  }
}
